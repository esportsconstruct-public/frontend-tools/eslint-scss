/**
 * @fileoverview Configuration applied when a user configuration extends from
 * eslint:recommended.
 * @author Nicholas C. Zakas
 */

"use strict";

module.exports = {
    overrides: [{
        files: ["*.component.html"],
        extends: ["plugin:@esportsconstruct-public/scss/recommended"],
        parserOptions: {
            templateSubfolders: ["project1", "project2"],
            excludeStartWith: ["g-", "ng-content-"]
        },
        rules: {
            "@esportsconstruct-public/scss/required-directive-for-children": [
                "error",
                {
                    directive: "useResponsive",
                    childrenStartWith: ["@include v-"],
                    modifierFromPseudo: true,
                    allowDeepNestedModifiers: true
                }
            ],
            "@esportsconstruct-public/scss/forbidden-children-for-host": [
                "error",
                {
                    childrenStartWith: ["@include v-"],
                    modifierFromPseudo: true,
                    allowDeepNestedModifiers: true
                }
            ]
        }
    }]
};
