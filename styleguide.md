# Styling Guidelines (Angular Element Modifier)

## What are we trying to achieve

Each selector must be found as quickly as possible. One block in styles describes one HTML element. One HTML element described in one place in styles.
We quickly find the problem, quickly fix it. It is possible to establish general rules and check.

## About names

We subdivide css classes into 2 types, almost like BEM
1. Main class
2. Modifier

Example
```html
<div class="container active"></div>
```

`container` is main class, `active` is modifier
Modifiers cannot exist without a main class
The main class cannot be a modifier for another main class

## Modifier's rules

1. Styles in different blocks can duplicate each other. Don't use grouping. A few extra bytes is better than the difficulty of determining. See [`@esportsconstruct-public/scss/forbidden-combined`](./packages/eslint-plugin-scss/src/docs/rules/forbidden-combined.md)

2. Nesting. See [`@esportsconstruct-public/scss/no-unnecessary-classes`](./packages/eslint-plugin-scss/src/docs/rules/wrong-scss-modifier.md)
      * Nesting styles can be written. The most common cases to use nesting are:
           * For pseudo-classes/-elements: `:first-child`, `:hover`, `::after`, etc.
           * For modifier classes like `.active` or `.disabled`.
      * In ideal world nesting deep shouldn't be more than 3.
      * Don't use nesting syntax to chain class names. Styles written like that is hard to maintain. You have to read throught all file to figure out which class these styles applied to. Also it's impossible to use search to find styles for a specific class name in that case.
        Example of **incorrect** code for this rule:

        ```scss
        .team {
               &-active { }

               &-header {
                   &-icon { }
               }
           }
        ```

        Example of **correct** code for this rule:

        ```scss
        .team {
               &.active { }

               &:hover { }
           }

        .team-header { }

        .team-header-icon { }
        ```

3. Forbidden to bind attribute class. See [`@esportsconstruct-public/scss/no-bind-class`](./packages/eslint-plugin-scss/src/docs/rules/no-bind-class.md)

## Environment rules

1. Put global/common styles in `src/styles/main.scss`
    * This can be broken down into multiple partials in the `src/styles` folder.
    * These files should follow the rules:
        * If a style/class is not usable on ~80% of all pages, then it shouldn't be in these files.
        * All global styles must be prefixed with `g-`
        * All global element styles can use prefix from parent class, like `g-card-content`, `g-card-img`.
        * If you had to write a global style, think about whether you can create a component-wrapper for this.
2. Put all the other styles in component SCSS files.
    * "Page" components should have limited styles, mostly positioning other components using CSS Grid, etc.
    * Other components should use `:host` to style the full component, and should contain semantic elements like lists, header/footer, etc. wherever possible. If there is no existing semantic element, then use a simple class on a div or span.

3. Use letters and hyphens only, no underscores, no double-hyphens, etc.
    * e.g. `match`, `opponent`, `tournament`, `team`, etc.
    * Modifier styles like `active`. Should be `match.active`. Wrong examples: `match-active`, `match__active`, `active`
    * Element styles like `content`. Should be just `.content`. Wrong examples: `card-content`, `card--content`.  Excluding global styles. Global styles example: `g-card-content`, `g-card-img`. This will prevent mixing.

4. Components shouldn't contain empty style files.

5. no ID selectors

6. Components shouldn't define their external geometry. Don't use margins to style `:host`. If a component needs to be positioned in some place define these styles in the outer component.
    Example of **incorrect** code for this rule:
    ```scss
    :host {
        display: block;
        margin-top: 10px;
    }
    ```
    Example of **correct** code for this rule:
    ```scss
    /** app-match-card **/
    :host {
        display: block;
    }
    ```

    ```html
    <app-match-card *ngFor="let matchCard of matchCards" class="match-card"></app-match-card>
    ```
    ```scss
    .match-card {
        /* We need 1rem here, but in other place where we use app-match-card we may need different margins. */
        margin-top: 1rem;
    }
    ```

7. All themable colours and fonts should use Sass variables.

8. Most likely you don't need to use `font-family` property. It is already installed in `html { font-family: $font-primary }`.

9. We use `rem` units for all css properties. Every project should have `html { font-size: 62.5%; }` style then `1rem = 10px`.
