<h1 align="center">Angular ESLint SCSS</h1>

<p align="center">Monorepo for all the tooling which enables ESLint for SCSS to lint Angular projects</p>

<p align="center">
    <a href="https://gitlab.com/esportsconstruct-public/frontend-tools/eslint-scss/-/packages"><img src="https://img.shields.io/npm/v/@esportsconstruct-public/template-parser/latest?label=template-parser&registry_uri=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fpackages%2Fnpm%2F" alt="NPM Downloads" /></a>
    <a href="https://gitlab.com/esportsconstruct-public/frontend-tools/eslint-scss/-/packages"><img src="https://img.shields.io/npm/v/@esportsconstruct-public/eslint-plugin-scss/latest?label=eslint-plugin-scss&registry_uri=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fpackages%2Fnpm%2F" alt="NPM Downloads" /></a>
    <a href="http://commitizen.github.io/cz-cli/"><img src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square" alt="Commitizen friendly" /></a>
</p>

Checking component styles according to [styleguide](./styleguide.md)


## Packages included in this project

Please follow the links below for the packages you care about.

- [`@esportsconstruct-public/template-parser`](./packages/template-parser/README.md) - An ESLint-specific parser which leverages the `@angular/compiler` to allow for custom ESLint rules to be written which assert things about your Angular templates.

- [`@esportsconstruct-public/eslint-plugin-scss`](./packages/eslint-plugin-scss/README.md) - An ESLint-specific plugin which, when used in conjunction with `@esportsconstruct-public/template-parser`, allows for Angular template-scss-specific linting rules to run.

## Installation and Setup

See [`@esportsconstruct-public/eslint-plugin-scss`](./packages/eslint-plugin-scss/README.md)



Inspired by this [angular-eslint](https://github.com/angular-eslint/angular-eslint/tree/master/packages/template-parser) (can work simultaneously)
