# Eslint plugin scss

Plugin for Eslint. Serves for the analysis of HTML and CSS files for Angular projects.
Finds unused styles, displays errors in CSS according to [styleguide](../../styleguide.md)

## How it works

The plugin reacts to the *component.html files, receives a tree of elements from the compiler's angular.
Searches for a *component.scss file in the neighborhood and determines its structure.

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```
Next add .npmrc file to root directory for your project (or add new line if you have already)
We use gilab npm registry, your npm needs to know where to find the package

```
@esportsconstruct-public:registry=https://gitlab.com/api/v4/packages/npm/
```

Next, install `template-parser` and `eslint-plugin-scss`:

```
$ npm install @esportsconstruct-public/template-parser --save-dev
$ npm install @esportsconstruct-public/eslint-plugin-scss --save-dev
```

## Usage

You need to put all your rules in the overrides block, because otherwise the rules for TS will be applied to HTML and vice versa.

```js
module.exports = {
    rules: [],
    overrides: [
        {
            files: ["*.ts"],
            rules: [/** Your rules here **/]
        },
    ]
}
```

Add new section to your overrides for `*component.html` files

```js
module.exports = {
    rules: [],
    overrides: [
        {
            files: ["*.ts"],
            rules: [/** Your rules here **/]
        },
        {
            files: [
                "*component.html"
            ],
            extends: ["plugin:@esportsconstruct-public/scss/recommended"]
        }
    ]
}
```


Then you can configure the rules you want to use under the rules section.

```js
module.exports = {
    rules: [],
    overrides: [
        {
            files: ["*.ts"],
            rules: [/** Your rules here **/]
        },
        {
            files: [
                "*component.html"
            ],
            parser: "@esportsconstruct-public/template-parser",
            plugins: ["@esportsconstruct-public/scss"],
            rules: {
                "@esportsconstruct-public/scss/no-bind-class": "error",
                "@esportsconstruct-public/scss/forbidden-combined": "error",
                "@esportsconstruct-public/scss/wrong-scss-modifier": "error",
                "@esportsconstruct-public/scss/no-unnecessary-classes": [
                    "error",
                    {
                        modifierFromPseudo: true,
                        allowDeepNestedModifiers: true,
                        excludeStartWith: ["g-"]
                    }
                ],
                "@esportsconstruct-public/scss/required-directive-for-mixin": [
                    "error",
                    {
                        selector: "useResponsive",
                        mixinStartWith: ["v-"],
                        modifierFromPseudo: true,
                        allowDeepNestedModifiers: true,
                        excludeStartWith: ["g-", "ng-content-"]
                    }
                ]
            }
        }
    ]
}
```

## Supported Rules

* Don't use undescribed css selectors [`@esportsconstruct-public/scss/no-unnecessary-classes`](./src/docs/rules/no-unnecessary-classes.md)
* Blocks cannot be combined [`@esportsconstruct-public/scss/forbidden-combined`](./src/docs/rules/wrong-scss-modifier.md)
* Wrong SCSS modifier [`@esportsconstruct-public/scss/no-unnecessary-classes`](./src/docs/rules/no-unnecessary-classes.md)
* Forbidden to bind attribute class [`@esportsconstruct-public/scss/no-bind-class`](./src/docs/rules/no-bind-class.md)
* Required directive for mixin uses [`@esportsconstruct-public/scss/required-directive-for-mixin`](./src/docs/rules/required-directive-for-mixin.md)





