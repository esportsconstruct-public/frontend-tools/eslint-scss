# Required directive for mixins (@esportsconstruct-public/scss/required-directive-for-mixin)

This rule finds HTML elements with a directive and checks that there is a mixin for this element in the SCSS. And vice versa. If there is a mixin there should be a directive. Because eslint does not allow simultaneous analysis of two files at once, then some errors begin with `[CSS *:*]`, which indicates that there is an error in the SCSS file

## Rule Details

This rule is aimed at finding unused directive and mixins.
The directive was found in HTML, but mixin is not exist in SCSS

Example of analyze **SCSS** file

```
[CSS 21:1] Directive useResponsive not found for class .article-title  @esportsconstruct-public/scss/required-directive-for-mixin
```

Example of analyze **HTML** file
```
Mixin not found for element div with class z. Remove not used directive  @esportsconstruct-public/scss/required-directive-for-mixin
```

### Options

* **modifierFromPseudo** boolean (default: true)
If the value is `false`, this code will generate an error that the `.active` modifier was not found.

```html
<div class="about active"></div>
```
```scss
.about:not(.active) { }
```

* **allowDeepNestedModifiers** boolean (default: true)
If the value is `false`, this code will generate an error that the `.active` modifier was not found. But sometimes there is no other way to describe a modifier that can only be turned on when another modifier is activated

```html
<div class="about has-hover active"></div>
```
```scss
.about {
  &.has-hover {
    &.active { }
  }
}
```

* **excludeStartWith** string[] (default: [])
Ignores selectors that start with any of the elements in this array.
In the example below, it is `["g-"]`. Lint will search only `name` but not for its modifiers `g-ellipsis` and not class `g-layout`
```html
<div class="g-layout"></div>
<div class="name g-ellipsis"></div>
```


* **mixinStartWith** string[] (default: [])
  Trying to find exactly those mixins that start with value.
  In the example, it is `["v-"]`
```html
<div class="example" useResponsive></div>
```
Passed example
```scss
.example {
  @include v-any {
  }
}
```
Wrong example
```scss
.example {
  @include any {
  }
}
```

* **selector** string (default: "")
  Selector for directive. `useResponsive` in next example
```html
<div class="example" useResponsive></div>
```
