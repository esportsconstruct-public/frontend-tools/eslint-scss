# Don't use undescribed css selectors (@esportsconstruct-public/scss/no-unnecessary-classes)

This rule looks for unused styles in HTML files and unused selectors in SCSS files. Because eslint does not allow simultaneous analysis of two files at once, then some errors begin with `[CSS *:*]`, which indicates that there is an error in the SCSS file

## Rule Details

This rule is aimed at finding unused styles.
The class was found in one of the files, but it is not used

Example of analyze **SCSS** file

```
[CSS 9:1] Not found class .refresh-icon in html file  @esportsconstruct-public/scss/no-unnecessary-classes
```

Example of analyze **HTML** file
```
Not found class .markets in scss file for element app-market-box  @esportsconstruct-public/scss/no-unnecessary-classes
```

### Options

* **modifierFromPseudo** boolean (default: true)
If the value is `false`, this code will generate an error that the `.active` modifier was not found.

```html
<div class="about active"></div>
```
```scss
.about:not(.active) { }
```

* **allowDeepNestedModifiers** boolean (default: true)
If the value is `false`, this code will generate an error that the `.active` modifier was not found. But sometimes there is no other way to describe a modifier that can only be turned on when another modifier is activated

```html
<div class="about has-hover active"></div>
```
```scss
.about {
  &.has-hover {
    &.active { }
  }
}
```

* **excludeStartWith** string[] (default: [])
Ignores selectors that start with any of the elements in this array.
In the example below, it is `["g-"]`. Lint will search only `name` but not for its modifiers `g-ellipsis` and not class `g-layout`
```html
<div class="g-layout"></div>
<div class="name g-ellipsis"></div>
```
