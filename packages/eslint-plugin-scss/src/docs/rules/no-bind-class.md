# Forbidden to bind attribute class (@esportsconstruct-public/scss/no-bind-class)

Angular compiler defines the class attribute as a `BoundAttribute` and not as an `Attribute`.
Rule`no-unnecessary-classes` will not work correctly with this rule.


## Rule Details

Example of **incorrect** code for this rule:

```ts
@Input() public type: 'defailt' | 'info' | 'warning' = 'defailt'
```
```html
<div class="container {{ type }}"></div>
```

Example of **correct** code for this rule:

```ts
@Input() public type: 'defailt' | 'info' | 'warning' = 'defailt'
```
```html
<div class="container" [class.info]="type === 'info'" [class.warning]="type === 'warning'"></div>
```

## When Not To Use It

You can turn this rule off if your code cannot have such a case, otherwise `no-unnecessary-classes` rule will not work correctly, because the angular compiler defines the class attribute as a `BoundAttribute` and not as an `Attribute`.
