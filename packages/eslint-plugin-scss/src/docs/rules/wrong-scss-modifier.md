# Wrong SCSS modifier (@esportsconstruct-public/scss/no-unnecessary-classes)

The modifier must always be applied to the class.
If it describes another block, it shouldn't be inside main class because they are not related and can be named differently.

## Rule Details

Example of **incorrect** code for this rule:

```html
<div class="container active"></div>
```
```scss
.container {
  .active { }

  &-disabled { }
}
```

Example of **correct** code for this rule:

```html
<div class="container active"></div>
```
```scss
.container {
  &.active { }

  &.disabled { }
}
```

## When Not To Use It

You can turn this rule off if you don't want to control modifiers
