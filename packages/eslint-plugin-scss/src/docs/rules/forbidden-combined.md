# Blocks cannot be combined (@esportsconstruct-public/scss/forbidden-combined)

This rule is aimed at quickly finding a selector.
It makes no sense to save the size of the style file at the expense of the readability of the code.
One HTML element is described in one place in one block of the SCSS file.

## Rule Details

Example of **error**
```
[CSS 42:1] Blocks cannot be combined. Line: .nickname,.about  @esportsconstruct-public/scss/forbidden-combined
```

Example of **incorrect** code for this rule:

```scss
.about {
    margin-left: 2.4rem;
}

.nickname,
.about {
    white-space: nowrap;
}

.nickname {
    font-size: 1.6rem;
}
```

Example of **correct** code for this rule:

```scss
.about {
    margin-left: 2.4rem;
    white-space: nowrap;
}

.nickname {
    font-size: 1.6rem;
    white-space: nowrap;
}
```

Example of **correct** code with pseudo selectors for this rule:

```scss
.about {
   &.active,
   &:hover {}
}
```
