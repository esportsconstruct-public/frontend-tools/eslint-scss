/**
 * @fileoverview no-unnecessary-classes
 * @author Peter Startsev
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

var rule = require("@esportsconstruct-public/eslint-plugin-scss/lib/rules/no-unnecessary-classes"),

    RuleTester = require("lib/api").RuleTester;


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

var ruleTester = new RuleTester();
ruleTester.run("no-unnecessary-classes", rule, {

    valid: [

        // give me some code that won't trigger a warning
    ],

    invalid: [
        {
            code: "no",
            errors: [{
                message: "Fill me in.",
                type: "Me too"
            }]
        }
    ]
});
