
import { RuleModule } from "@typescript-eslint/experimental-utils/dist/ts-eslint";

export interface Rule<M extends string, O extends readonly unknown[]> {
    name: string;
    createESLintRule: RuleModule<M, O>
}
