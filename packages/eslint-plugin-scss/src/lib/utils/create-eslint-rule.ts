import { ESLintUtils } from '@typescript-eslint/experimental-utils';
import {ParserServices} from "@esportsconstruct-public/template-parser/dist/interfaces/parser-result";

export const createESLintRule = ESLintUtils.RuleCreator(() => ``);

export function getTemplateParserServices(context: any): ParserServices {
  if (
    !context.parserServices ||
    !(context.parserServices as any).defineTemplateBodyVisitor ||
      !(context.parserServices as any).convertNodeSourceSpanToLoc ||
    !(context.parserServices as any).getScssParser
  ) {
    /**
     * The user needs to have configured "parser" in their eslint config and set it
     * to template-parser
     */
    throw new Error(
      "You have used a rule which requires '@esportsconstruct-public/template-parser' to be used as the 'parser' in your ESLint config.",
    );
  }
  return context.parserServices;
}
