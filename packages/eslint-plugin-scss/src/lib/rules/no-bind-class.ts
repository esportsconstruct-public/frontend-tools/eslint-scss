/**
 * @fileoverview no-unnecessary-classes
 * @author Peter Startsev
 */

"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------
//

import {
    getTemplateParserServices,
    createESLintRule,
} from "../utils/create-eslint-rule";
import { BoundAttribute } from "@angular/compiler/src/render3/r3_ast";
import { BindingType, AST } from "@angular/compiler/src/expression_parser/ast";
import {ParserNode} from "@esportsconstruct-public/template-parser/dist/interfaces/parser-node";

type Options = [
    {
        maxComplexity: number;
    }
];

export type MessageIds = "notBindClass";

export default createESLintRule<Options, MessageIds>({
    name: "no-bind-class",
    defaultOptions: [
        {
            maxComplexity: 5,
        },
    ],
    meta: {
        type: "suggestion",
        docs: {
            description: "Forbidden to bind attribute class",
            category: "Best Practices",
            recommended: "error",
        },
        fixable: "code", // or "code" or "whitespace"
        schema: [
            {
                type: "array",
                items: {
                    type: "string",
                    minLength: 1
                },
                uniqueItems: true
            },
        ],
        messages: {
            notBindClass:
                "Forbidden to bind attribute class (class=\"{{ value }}\") for {{ parent }}. ",
        },
    },

    create(context) {
        const parserServices = getTemplateParserServices(context);

        return parserServices.defineTemplateBodyVisitor({
            BoundAttribute(node: BoundAttribute & ParserNode) {
                if ((node as BoundAttribute).name === 'class' && (node as {angularBindingType: BindingType}).angularBindingType === BindingType.Property) {
                    const loc = parserServices.convertNodeSourceSpanToLoc(
                        (node as BoundAttribute).sourceSpan
                    );
                    context.report({
                        messageId: "notBindClass",
                        loc,
                        data: {
                            value: ((node as BoundAttribute).value as AST & {source: string})?.source ?? '' ,
                            parent: (node as ParserNode).parent?.name ?? (node as ParserNode).parent?.tagName,
                        },
                    });
                }
            },
        }) as any;
    },
});
