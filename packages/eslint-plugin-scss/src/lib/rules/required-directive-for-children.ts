/**
 * @fileoverview no-unnecessary-classes
 * @author Peter Startsev
 */

"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------
//

import {createESLintRule, getTemplateParserServices} from "../utils/create-eslint-rule";
import {Element, TextAttribute} from "@angular/compiler/src/render3/r3_ast";
import {ScssTree} from "../../../../../../../_esportsconstruct/eslint-scss/packages/template-parser/dist/scss-parser";
import {ReportDescriptor, RuleFix} from "@typescript-eslint/experimental-utils/dist/ts-eslint/Rule";

type Options = [
    {
        directive: string;
        childrenStartWith: string[];
        allowDeepNestedModifiers: boolean; // .class ( &.modifier { &.modifier-2 { ... } } }
        modifierFromPseudo: boolean; // :not(.modifier)
    }
];

export type MessageIds = "classAttributeNotFound" | "classNotFound" | "directiveNotFound" | "cssChildrenNotFound";

function ignoreSelector(selector: string): boolean {
    return (
        selector.trim().startsWith("&:") ||
        selector.trim().startsWith(":") ||
        selector.trim().startsWith("&.")
    );
}

export default createESLintRule<Options, MessageIds>({
    name: "required-directive-for-children",
    defaultOptions: [
        {
            directive: "useResponsive",
            childrenStartWith: ["v-"],
            modifierFromPseudo: true,
            allowDeepNestedModifiers: true,
        },
    ],
    meta: {
        type: "suggestion",
        docs: {
            description: "Required directive for css children",
            category: "Best Practices",
            recommended: "error",
        },
        fixable: "code", // or "code" or "whitespace"
        schema: [
            {
                type: "object",
                properties: {
                    directive: { // :not(.modifier)
                        type: "string",
                        default: "useResponsive",
                    },
                    childrenStartWith: {
                        type: "array",
                        items: {
                            type: "string",
                            minLength: 1,
                        },
                        uniqueItems: true,
                    },
                    modifierFromPseudo: { // :not(.modifier)
                        type: "boolean",
                        default: true,
                    },
                    allowDeepNestedModifiers: {
                        type: "boolean",
                        default: true,
                    },
                },
                additionalProperties: false,
            },
        ],
        messages: {
            classNotFound:
                "Not found class .{{ class }} in scss file for element {{ parent }}",
            classAttributeNotFound:
                "Class attribute not found for element {{elementName}} in HTML file, but directive {{directiveName}} exist",
            directiveNotFound:
                "[CSS {{ line }}:1] Directive {{directiveName}} not found for class {{class}} [{{filePath}}]",
            cssChildrenNotFound:
                "Css children not found for element {{elementName}} with class {{className}}. Remove not used directive",
        },
    },

    create(context) {
        const parserServices = getTemplateParserServices(context);
        const scssParsers = parserServices.getScssParser();
        const htmlTree = parserServices.getHtmlTree();

        return parserServices.defineTemplateBodyVisitor({
            TextAttribute(node: TextAttribute & { parent: Element }) {


                const childrenNotFoundMixinsReports: Array<ReportDescriptor<MessageIds>> = [];
                const childrenFoundMixins: Array<ReportDescriptor<MessageIds>> = [];

                scssParsers.forEach(scssParsersForProject => {
                    let hasMixin = false;

                    if (scssParsersForProject.find(scssParser => scssParser.ignoreParsing)) {
                        return;
                    }


                    if (node.name !== context
                        .options?.[0]
                        .directive) {
                        return;
                    }

                    // @ts-ignore
                    const loc = parserServices.convertNodeSourceSpanToLoc(
                        node.sourceSpan,
                    );


                    const originalClasses: string[] | undefined = node.parent.attributes.find(attribute => attribute.name === "class")?.value.split(" ");
                    let HtmlMainClass: string = "";
                    let classes: string[] = [];
                    if (originalClasses) {
                        const ignoreFromOptions: string[] | null =
                            htmlTree.excludeStartWith ?? [];
                        if (ignoreFromOptions) {
                            classes = classes.filter(
                                (item) =>
                                    !ignoreFromOptions.find((ignore) =>
                                        item.startsWith(ignore),
                                    ),
                            );
                        }
                        classes = originalClasses;

                        HtmlMainClass = classes.length ? classes[0] : "";
                    }


                    if (!originalClasses || !HtmlMainClass) {
                        context.report({
                            messageId: "classAttributeNotFound",
                            loc,
                            data: {
                                directiveName: context.options?.[0].directive,
                                elementName: node.parent.name,
                            },
                        });
                        return;
                    }

                    // @ts-ignore
                    let classFound = false;
                    let mainSelectors: ScssTree[] = [];
                    scssParsersForProject.forEach(scssParser => {
                        mainSelectors = [...mainSelectors, ...scssParser.tree.filter((child) =>
                            child.selectorWithoutPseudo
                                .split(",")
                                .find((child) => !ignoreSelector(child)),
                        )];
                    });

                    mainSelectors.forEach((mainSelector) => {
                        mainSelector.selectorWithoutPseudo
                            .split(",")
                            .forEach((mainClass) => {
                                if (mainClass.substring(1) === HtmlMainClass) {
                                    classFound = true;
                                }
                            });
                    });

                    if (!classFound) {
                        context.report({
                            messageId: "classNotFound",
                            loc,
                            data: {
                                class: HtmlMainClass,
                                parent: node.parent.name,
                            },
                        });
                    } else {
                        mainSelectors.forEach((mainSelector) => {
                            mainSelector.selectorWithoutPseudo
                                .split(",")
                                .filter(
                                    (selector) =>
                                        selector.substring(1) === HtmlMainClass,
                                )
                                .forEach(() => {
                                    if (mainSelector.children) {
                                        mainSelector.children.forEach((children) => {
                                            if (context.options[0].childrenStartWith.some(mixin => children.selector.startsWith(mixin))) {
                                                hasMixin = true;
                                            }
                                        });
                                    }
                                });
                        });
                    }

                    if (!hasMixin) {
                        childrenNotFoundMixinsReports.push(
                            {
                                messageId: "cssChildrenNotFound",
                                loc,
                                data: {
                                    elementName: node.parent.name,
                                    className: HtmlMainClass,
                                },
                                // @ts-ignore
                                fix: function (fixer) {
                                    const node = htmlTree.tree.find(item => item.mainClass === HtmlMainClass.replace(".", ""));
                                    const fixers: RuleFix[] = [];
                                    if (node) {
                                        node.elements.forEach(element => {
                                            if (element?.node) {
                                                const sourceSpan = (element.node.attributes.find(attribute => attribute.name === context.options?.[0].directive) as TextAttribute | undefined)?.sourceSpan;
                                                if (sourceSpan) {
                                                    const start = sourceSpan.start.offset;
                                                    const end = sourceSpan.end.offset;

                                                    if (start && end) {
                                                        fixers.push(fixer.removeRange([start, end]));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    return fixers;
                                },
                            },
                        );
                    } else {
                        childrenFoundMixins.push({messageId: "cssChildrenNotFound", loc})
                    }
                });

                childrenNotFoundMixinsReports.filter(report => {
                    const found = childrenFoundMixins.find(child => child.messageId === "cssChildrenNotFound" && JSON.stringify(child.loc) === JSON.stringify(report.loc))
                    return !found;
                }).forEach(report => context.report(report));
            },
            "Program:exit"() {
                scssParsers.forEach(scssParsersForProject => {

                    if (scssParsersForProject.find(scssParser => scssParser.ignoreParsing)) {
                        return;
                    }

                    const loc = {
                        start: {
                            line: 1,
                            column: 1,
                        },
                        end: {
                            line: 1,
                            column: 1,
                        },
                    };

                    let mainSelectors: Array<{ selectors: ScssTree[], path: string }> = [];
                    scssParsersForProject.forEach(scssParser => {
                        mainSelectors = [...mainSelectors, {
                            selectors: scssParser.tree.filter((child) => child.selectorWithoutPseudo
                                .split(",")
                                .find((child) => !ignoreSelector(child)),
                            ), path: scssParser.path,
                        }];
                    });

                    mainSelectors.forEach((mainSelectorItem) => {
                        mainSelectorItem.selectors.forEach((mainSelector) => {
                            mainSelector.selectorWithoutPseudo
                                .split(",")
                                .forEach((mainClass) => {
                                    const foundMainClassFrom = htmlTree.tree.find(
                                        (classFromHtml) =>
                                            classFromHtml.mainClass ===
                                            mainClass.substring(1),
                                    );

                                    if (!foundMainClassFrom) {

                                    } else {
                                        mainSelector.children
                                            ?.filter((child) =>
                                                child.selectorWithoutPseudo
                                                    .split(",")
                                                    .find(
                                                        (child) =>
                                                            !ignoreSelector(child),
                                                    ),
                                            )
                                            .forEach((child) => {
                                                child.selectorWithoutPseudo
                                                    .split(",")
                                                    .forEach((modifierClass) => {
                                                        if (context.options[0].childrenStartWith.some(mixin => modifierClass.startsWith(mixin))) {
                                                            const node = htmlTree.tree.find(item => item.mainClass === mainClass.replace(".", ""));
                                                            if (node) {
                                                                node.elements.forEach(element => {

                                                                    if (!element.attributes.find(
                                                                        (attribute) => attribute === context.options?.[0].directive,
                                                                    )) {

                                                                        context.report({
                                                                            messageId:
                                                                                "directiveNotFound",
                                                                            loc,
                                                                            data: {
                                                                                directiveName: context.options?.[0].directive,
                                                                                class: mainClass,
                                                                                filePath: mainSelectorItem.path,
                                                                                line:
                                                                                mainSelector
                                                                                    .loc
                                                                                    .start
                                                                                    .line,
                                                                            },
                                                                            // @ts-ignore
                                                                            fix: function (fixer) {

                                                                                const fixers: RuleFix[] = [];

                                                                                if (node) {

                                                                                    if (element?.node) {
                                                                                        const offset = element.node.startSourceSpan?.end.offset;
                                                                                        if (offset) {
                                                                                            const offsetChar = ["img", "hr", "br"].includes(element.node.name) ? 2 : 1;
                                                                                            fixers.push(fixer.insertTextAfterRange([offset - offsetChar, offset - offsetChar], " useResponsive"));
                                                                                        }
                                                                                    }

                                                                                }

                                                                                return fixers;
                                                                            },
                                                                        });
                                                                    }

                                                                });
                                                            }
                                                        }

                                                    });
                                            });
                                    }
                                });
                        });

                    });

                });
            },
        }) as any;
    },
});
