/**
 * @fileoverview no-unnecessary-classes
 * @author Peter Startsev
 */

"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------
//

import {createESLintRule, getTemplateParserServices} from "../utils/create-eslint-rule";

type Options = [
    {
        maxComplexity: number;
    }
];

export type MessageIds = "wrongModifier";

// const BOUND_ATTRIBUTE_NAMES = ['ngForOf', 'ngIf', 'ngSwitchCase'];
// const TEXT_ATTRIBUTE_NAMES = ['ngSwitchDefault'];

function ignoreSelector(selector: string): boolean {
    return (
        selector.trim().startsWith("&:") ||
        selector.trim().startsWith(":") ||
        selector.trim().startsWith("@")
    );
}

export default createESLintRule<Options, MessageIds>({
    name: "no-unnecessary-classes",
    defaultOptions: [
        {
            maxComplexity: 5,
        },
    ],
    meta: {
        type: "suggestion",
        docs: {
            description: "The modifier must always be applied to the class.",
            category: "Best Practices",
            recommended: "error",
        },
        fixable: "code", // or "code" or "whitespace"
        schema: [
            {
                type: "array",
                items: {
                    type: "string",
                    minLength: 1,
                },
                uniqueItems: true,
            },
        ],
        messages: {
            wrongModifier:
                "[CSS {{ line }}:1] Wrong modifier {{ modifier }} for class {{ class }}. You forgot '&'? [{{filePath}}]",
        },
    },

    create(context, _z) {
        const parserServices = getTemplateParserServices(context);
        const scssParsers = parserServices.getScssParser();

        return parserServices.defineTemplateBodyVisitor({
            "Program:exit"() {
                scssParsers.forEach(scssParsersForProject => {

                    if (scssParsersForProject.find(scssParser => scssParser.ignoreParsing)) {
                        return;
                    }

                    const loc = {
                        start: {
                            line: 1,
                            column: 1,
                        },
                        end: {
                            line: 1,
                            column: 1,
                        },
                    };

                    scssParsersForProject.forEach(scssParser => {
                        scssParser.tree
                            .filter(
                                (mainClass) => !ignoreSelector(mainClass.selector),
                            )
                            .forEach((mainClass) => {
                                mainClass.children?.filter(
                                    (child) => !ignoreSelector(child.selector),
                                ).filter((child) => child.selector.startsWith("."))
                                    .forEach((child) => {
                                        context.report({
                                            messageId: "wrongModifier",
                                            loc,
                                            data: {
                                                line: child.loc.start.line,
                                                modifier: child.selector.trim(),
                                                class: mainClass.selector.trim(),
                                                filePath: scssParser.path
                                            },
                                        });
                                    });
                            });
                    });
                });
            },
        }) as any;
    },
});
