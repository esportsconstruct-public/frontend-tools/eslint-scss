/**
 * @fileoverview no-unnecessary-classes
 * @author Peter Startsev
 */

"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------
//

import {
    getTemplateParserServices,
    createESLintRule,
} from "../utils/create-eslint-rule";

type Options = [];

export type MessageIds = "hasCombinedStyle" | "hasWrongNested";

function ignoreSelector(selector: string): boolean {
    return (
        selector.trim().startsWith("&:") ||
        selector.trim().startsWith(":") ||
        selector.trim().startsWith("@")
    );
}

export default createESLintRule<Options, MessageIds>({
    name: "forbidden-combined",
    defaultOptions: [],
    meta: {
        type: "suggestion",
        docs: {
            description: "Blocks cannot be combined",
            category: "Best Practices",
            recommended: "error",
        },
        fixable: "code", // or "code" or "whitespace"
        schema: [],
        messages: {
            hasCombinedStyle:
                "[CSS {{ line }}:{{ col }}] Blocks cannot be combined. Line: {{ selector }} [{{filePath}}]",
            hasWrongNested:
                "[CSS {{ line }}:{{ col }}] Blocks cannot be nested. Line: {{ selector }} [{{filePath}}]",
        },
    },

    create(context) {
        // let totalComplexity = 0;

        const parserServices = getTemplateParserServices(context);
        const scssParsers = parserServices.getScssParser();

        return parserServices.defineTemplateBodyVisitor({
            "Program:exit"() {
                scssParsers.forEach(scssParsersForProject => {
                    if (scssParsersForProject.find(scssParser => scssParser.ignoreParsing)) {
                        return;
                    }

                    const loc = {
                        start: {
                            line: 1,
                            column: 1,
                        },
                        end: {
                            line: 1,
                            column: 1,
                        },
                    };

                    scssParsersForProject.forEach(scssParser => {
                        scssParser.tree
                            .filter(
                                (child) =>
                                    child.selectorWithoutPseudo
                                        .split(",")
                                        .find((child) => !ignoreSelector(child))
                            )
                            .forEach((mainClass) => {


                                if (
                                    mainClass.selectorWithoutPseudo.split(",")
                                        .length > 1
                                ) {
                                    context.report({
                                        messageId: "hasCombinedStyle",
                                        loc,
                                        data: {
                                            line: mainClass.loc.start.line,
                                            col: mainClass.loc.start.col,
                                            selector: mainClass.selector.trim(),
                                            path: scssParser.path
                                        },
                                    });
                                }
                                if (
                                    mainClass.selectorWithoutPseudo.split(" ")
                                        .length > 1
                                ) {
                                    context.report({
                                        messageId: "hasWrongNested",
                                        loc,
                                        data: {
                                            line: mainClass.loc.start.line,
                                            col: mainClass.loc.start.col,
                                            selector: mainClass.selector.trim(),
                                            path: scssParser.path
                                        },
                                    });
                                }

                                mainClass.children
                                    ?.filter(
                                        (child) =>
                                            child.selectorWithoutPseudo
                                                .split(",")
                                                .find((child) => !ignoreSelector(child))
                                    )
                                    .forEach((child) => {
                                        if (
                                            child.selectorWithoutPseudo.split(
                                                ","
                                            ).length > 1
                                        ) {
                                            context.report({
                                                messageId: "hasCombinedStyle",
                                                loc,
                                                data: {
                                                    line: child.loc.start.line,
                                                    col: child.loc.start.col,
                                                    selector: child.selector.trim(),
                                                    path: scssParser.path
                                                },
                                            });
                                        }
                                        if (
                                            child.selectorWithoutPseudo.split(
                                                ">"
                                            ).find(name => name.trim().split(
                                                " "
                                            ).length > 1)
                                        ) {
                                            context.report({
                                                messageId: "hasWrongNested",
                                                loc,
                                                data: {
                                                    line: child.loc.start.line,
                                                    col: child.loc.start.col,
                                                    selector: child.selector.trim(),
                                                    path: scssParser.path
                                                },
                                            });
                                        }
                                    });

                            });
                    });
                });
            } ,
        }) as any;
    },
});
