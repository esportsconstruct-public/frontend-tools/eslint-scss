/**
 * @fileoverview no-unnecessary-classes
 * @author Peter Startsev
 */

"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------
//

import {
    createESLintRule,
    getTemplateParserServices,
} from "../utils/create-eslint-rule";

type Options = [
    {
        childrenStartWith: string[];
        allowDeepNestedModifiers: boolean; // .class ( &.modifier { &.modifier-2 { ... } } }
        modifierFromPseudo: boolean; // :not(.modifier)
    }
];

export type MessageIds = "foundMixinInHost";

export default createESLintRule<Options, MessageIds>({
    name: "forbidden-children-for-host",
    defaultOptions: [
        {
            childrenStartWith: ["v-"],
            modifierFromPseudo: true,
            allowDeepNestedModifiers: true,
        },
    ],
    meta: {
        type: "suggestion",
        docs: {
            description: "Forbidden css children for :host selector",
            category: "Best Practices",
            recommended: "error",
        },
        fixable: "code", // or "code" or "whitespace"
        schema: [
            {
                type: "object",
                properties: {
                    childrenStartWith: {
                        type: "array",
                        items: {
                            type: "string",
                            minLength: 1,
                        },
                        uniqueItems: true,
                    },
                    modifierFromPseudo: {
                        // :not(.modifier)
                        type: "boolean",
                        default: true,
                    },
                    allowDeepNestedModifiers: {
                        type: "boolean",
                        default: true,
                    },
                },
                additionalProperties: false,
            },
        ],
        messages: {
            foundMixinInHost:
                `[CSS {{ line }}:1] Forbidden to use children that start with "{{startWith}}" in the :host selector [{{filePath}}]`,
        },
    },

    create(context) {
        const parserServices = getTemplateParserServices(context);
        const scssParsers = parserServices.getScssParser();

        return parserServices.defineTemplateBodyVisitor({
            "Program:exit"() {
                scssParsers.forEach((scssParsersForProject) => {
                    if (
                        scssParsersForProject.find(
                            (scssParser) => scssParser.ignoreParsing
                        )
                    ) {
                        return;
                    }

                    const loc = {
                        start: {
                            line: 1,
                            column: 1,
                        },
                        end: {
                            line: 1,
                            column: 1,
                        },
                    };

                    scssParsersForProject.forEach((scssParser) => {
                        const hostSelector = scssParser.tree.find(
                            (element) => element.selector === ":host"
                        );
                        if (hostSelector?.children) {
                            context.options[0].childrenStartWith.forEach(
                                (startWith) => {
                                    if (hostSelector?.children) {
                                        if (
                                            hostSelector.children.find(
                                                (element) =>
                                                    element.selector.startsWith(
                                                        startWith
                                                    )
                                            )
                                        ) {
                                            context.report({
                                                messageId: "foundMixinInHost",
                                                loc,
                                                data: {
                                                    filePath: scssParser.path,
                                                    startWith,
                                                    line:
                                                        hostSelector.loc.start
                                                            .line,
                                                },
                                            });
                                        }
                                    }
                                }
                            );
                        }
                    });
                });
            },
        }) as any;
    },
});
