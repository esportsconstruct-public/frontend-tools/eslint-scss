/**
 * @fileoverview no-unnecessary-classes
 * @author Peter Startsev
 */

"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------
//

import {createESLintRule, getTemplateParserServices} from "../utils/create-eslint-rule";
import {Element, TextAttribute} from "@angular/compiler/src/render3/r3_ast";
import {BindingType} from "@angular/compiler/src/expression_parser/ast";
import {Rule} from "../../interfaces/rule";
import {ScssTree} from "@esportsconstruct-public/template-parser/dist/scss-parser";
import {LOC} from "@esportsconstruct-public/template-parser/dist/interfaces/ast";
import {ParserNode} from "@esportsconstruct-public/template-parser/dist/interfaces/parser-node";


type Options = [
    {
        modifierFromPseudo: boolean; // :not(.modifier)
        allowDeepNestedModifiers: boolean; // .class ( &.modifier { &.modifier-2 { ... } } }
    }
];

export type MessageIds =
    | "usedModifierWithoutMainClass"
    | "emptyClassAttribute"
    | "classNotFound"
    | "modifierNotFound"
    | "classNotFoundInHtml"
    | "modifierNotFoundInHtml";

function ignoreSelector(selector: string): boolean {
    return (
        selector.trim().startsWith("&:") ||
        selector.trim().startsWith(":") ||
        selector.trim().startsWith("@")
    );
}


export class noUnnecessaryRule implements Rule<MessageIds, Options> {
    public name = "no-unnecessary-classes";

    public createESLintRule = createESLintRule<Options, MessageIds>({
        name: this.name,
        defaultOptions: [
            {
                modifierFromPseudo: true,
                allowDeepNestedModifiers: true,
            },
        ],
        meta: {
            type: "suggestion",
            docs: {
                description: "This rule looks for unused styles in HTML files and unused selectors in SCSS files",
                category: "Best Practices",
                recommended: "error",
            },
            fixable: "code", // or "code" or "whitespace"
            schema: [
                {
                    type: "object",
                    properties: {
                        modifierFromPseudo: { // :not(.modifier)
                            type: "boolean",
                            default: true,
                        },
                        allowDeepNestedModifiers: {
                            type: "boolean",
                            default: true,
                        },
                    },
                    additionalProperties: false,
                },
            ],
            messages: {
                usedModifierWithoutMainClass:
                    "You use modifier [class.{{ modifier }}] but not use class for {{ element }}",
                emptyClassAttribute: "Found empty class=\"\" for {{ parent }}",
                classNotFound:
                    "Not found class .{{ class }} in scss file for element {{ parent }}",
                modifierNotFound:
                    "Not found modifier &.{{ modifier }} for class {{ class }} in scss file for element {{ parent }}",
                classNotFoundInHtml:
                    "[CSS {{ line }}:1] Not found class {{ class }} in html file [{{filePath}}]",
                modifierNotFoundInHtml:
                    "[CSS {{ line }}:1] Not found modifier {{ modifier }} for class {{ class }} in html file [{{filePath}}]",
            },
        },
        create(context) {
            const parserServices = getTemplateParserServices(context);
            const scssParsers = parserServices.getScssParser();
            const htmlTree = parserServices.getHtmlTree();

            const getChildrensInCss = (childrenSelectors: ScssTree[]): string[] => {
                let childrensInCss: string[] = [];
                childrenSelectors?.forEach(
                    (childrenSelector) => {

                        if (context
                            .options?.[0]
                            .allowDeepNestedModifiers && childrenSelector.children?.length) {
                            childrensInCss = [...childrensInCss, ...getChildrensInCss(childrenSelector.children)];
                        }

                        childrenSelector.selector
                            .split(",")
                            .forEach(
                                (modifierClass) => {
                                    if (
                                        context
                                            .options?.[0]
                                            .modifierFromPseudo
                                    ) {
                                        const reqexp = /\.[\w-]+/gu;
                                        let result;
                                        while (
                                            (result = reqexp.exec(
                                                modifierClass,
                                            ))
                                            ) {
                                            if (
                                                result &&
                                                !ignoreSelector(
                                                    result.toString(),
                                                )
                                            ) {
                                                childrensInCss.push(
                                                    `&${result.toString()}`,
                                                );
                                            }
                                        }
                                    } else {
                                        if (
                                            modifierClass &&
                                            !ignoreSelector(
                                                modifierClass,
                                            )
                                        ) {
                                            childrensInCss.push(
                                                modifierClass,
                                            );
                                        }
                                    }
                                },
                            );

                        // only pseudo in selector, need go in
                        if (
                            !childrenSelector.selectorWithoutPseudo
                        ) {
                            childrenSelector.children?.forEach(
                                (
                                    childInPseudoSelector,
                                ) => {
                                    childInPseudoSelector.selector
                                        .split(",")
                                        .forEach(
                                            (
                                                modifierClass,
                                            ) => {
                                                if (
                                                    context
                                                        .options?.[0]
                                                        .modifierFromPseudo
                                                ) {
                                                    const reqexp = /\.[\w-]+/gu;
                                                    let result;
                                                    while (
                                                        (result = reqexp.exec(
                                                            modifierClass,
                                                        ))
                                                        ) {
                                                        if (
                                                            result &&
                                                            !ignoreSelector(
                                                                result.toString(),
                                                            )
                                                        ) {
                                                            childrensInCss.push(
                                                                `&${result.toString()}`,
                                                            );
                                                        }
                                                    }
                                                } else {
                                                    if (
                                                        modifierClass &&
                                                        !ignoreSelector(
                                                            modifierClass,
                                                        )
                                                    ) {
                                                        childrensInCss.push(
                                                            modifierClass,
                                                        );
                                                    }
                                                }
                                            },
                                        );
                                },
                            );
                        }
                    },
                );
                return childrensInCss;
            };

            const existModifier = (HtmlModifier: string, mainSelector: ScssTree): boolean => {
                const childrenSelectors = mainSelector.children?.filter(
                    (child) =>
                        child.selectorWithoutPseudo.split(
                            ",",
                        ),
                );

                return !(childrenSelectors &&
                    !getChildrensInCss(childrenSelectors).find(
                        (child) =>
                            child.startsWith("&.") &&
                            child.substring(2) ===
                            HtmlModifier,
                    )
                )
            };

            return parserServices.defineTemplateBodyVisitor({
                TextAttribute(node: TextAttribute & { parent: Element }) {
                    const modifierNotFounds: {[key: string]: Array<{
                            messageId: MessageIds;
                            loc: {
                                start: LOC;
                                end: LOC;
                            },
                            data: {
                                modifier: string,
                                class: string,
                                parent: string,
                            },
                        }>} = {}
                    scssParsers.forEach(scssParsersForProject => {

                        if (scssParsersForProject.find(scssParser => scssParser.ignoreParsing) || node.name !== "class") {
                            return;
                        }

                        const loc = parserServices.convertNodeSourceSpanToLoc(
                            node.sourceSpan,
                        );

                        if (!node.value.split(" ").length) {
                            context.report({
                                messageId: "emptyClassAttribute",
                                loc,
                                data: {
                                    parent: node.parent.name,
                                },
                            });
                            return;
                        }




                        const originalClasses: string[] = node.value.split(" ");
                        let classes: string[] = originalClasses;

                        const ignoreFromOptions: string[] | null =
                            htmlTree.excludeStartWith ?? [];
                        if (ignoreFromOptions) {
                            classes = classes.filter(
                                (item) =>
                                    !ignoreFromOptions.find((ignore) =>
                                        item.startsWith(ignore),
                                    ),
                            );
                        }

                        const HtmlMainClass: string = classes.length ? classes[0] : "";

                        if (!HtmlMainClass) {
                            return;
                        }

                        const foundClassInputs = node.parent.inputs.filter(
                            (input) => input.type === BindingType.Class,
                        );
                        if (foundClassInputs?.length) {
                            classes = [
                                ...classes,
                                ...foundClassInputs.map((input) => input.name),
                            ];
                        }


                        const foundAttributeRouterLinkActive = node.parent.attributes.find(
                            (input) => input.name === "routerLinkActive",
                        );
                        if (foundAttributeRouterLinkActive) {
                            classes = [
                                ...classes,
                                foundAttributeRouterLinkActive.value,
                            ];
                        }

                        const foundInputRouterLinkActive = node.parent.inputs.find(
                            // @ts-ignore
                            (input) => (input as unknown as ParserNode).angularBindingType === BindingType.Property && input.name === "routerLinkActive",
                        );
                        const foundInputRouterLinkActiveValue = (foundInputRouterLinkActive as unknown as ParserNode)?.value;
                        if (foundInputRouterLinkActiveValue) {
                            classes = [
                                ...classes,
                                foundInputRouterLinkActiveValue.source.replace(/'/gu, ""),
                            ];
                        }

                        const HtmlModifiers: string[] = classes.length
                            ? classes.slice(1)
                            : [];


                        let classFound = false;
                        let mainSelectors: ScssTree[] = [];
                        scssParsersForProject.forEach(scssParser => {
                            mainSelectors = [...mainSelectors, ...scssParser.tree.filter((child) =>
                                child.selectorWithoutPseudo
                                    .split(",")
                                    .find((child) => !ignoreSelector(child)),
                            )];
                        });

                        mainSelectors.forEach((mainSelector) => {
                            mainSelector.selectorWithoutPseudo
                                .split(",")
                                .forEach((mainClass) => {
                                    if (mainClass.substring(1) === HtmlMainClass) {
                                        classFound = true;
                                    }
                                });
                        });

                        if (!classFound) {
                            context.report({
                                messageId: "classNotFound",
                                loc,
                                data: {
                                    class: HtmlMainClass,
                                    parent: node.parent.name,
                                },
                            });
                        } else {
                            mainSelectors.forEach((mainSelector) => {
                                mainSelector.selectorWithoutPseudo
                                    .split(",")
                                    .filter(
                                        (selector) =>
                                            selector.substring(1) === HtmlMainClass,
                                    )
                                    .forEach((mainClass) => {
                                        HtmlModifiers.forEach((HtmlModifier) => {
                                            if (!existModifier(HtmlModifier, mainSelector)) {
                                                if (!modifierNotFounds[`${HtmlModifier}${mainClass}${node.parent.name}`]) {
                                                    modifierNotFounds[`${HtmlModifier}${mainClass}${node.parent.name}`] = [];
                                                }

                                                modifierNotFounds[`${HtmlModifier}${mainClass}${node.parent.name}`].push({
                                                    messageId: "modifierNotFound",
                                                    loc,
                                                    data: {
                                                        modifier: HtmlModifier,
                                                        class: mainClass,
                                                        parent: node.parent.name,
                                                    },
                                                });
                                            }
                                        });
                                    });
                            });
                        }

                        Object.keys(modifierNotFounds).forEach((key) => {
                            if (modifierNotFounds[key].length && modifierNotFounds[key].length === scssParsers.length) {
                                context.report(modifierNotFounds[key][0]);
                            }
                        });
                    });
                },
                "Program:exit"() {
                    scssParsers.forEach(scssParsersForProject => {

                        if (scssParsersForProject.find(scssParser => scssParser.ignoreParsing)) {
                            return;
                        }

                        const loc = {
                            start: {
                                line: 1,
                                column: 1,
                            },
                            end: {
                                line: 1,
                                column: 1,
                            },
                        };

                        let mainSelectors: Array<{ selectors: ScssTree[], path: string }> = [];
                        scssParsersForProject.forEach(scssParser => {
                            mainSelectors = [...mainSelectors, {selectors: scssParser.tree.filter((child) => child.selectorWithoutPseudo
                                .split(",")
                                .find((child) => !ignoreSelector(child)),
                            ), path: scssParser.path}];
                        });

                        mainSelectors.forEach((mainSelectorItem) => {
                            mainSelectorItem.selectors.forEach((mainSelector) => {
                                mainSelector.selectorWithoutPseudo
                                    .split(",")
                                    .forEach((mainClass) => {
                                        const foundMainClassFrom = htmlTree.tree.find(
                                            (classFromHtml) =>
                                                classFromHtml.mainClass ===
                                                mainClass.substring(1),
                                        );

                                        if (!foundMainClassFrom) {
                                            context.report({
                                                messageId: "classNotFoundInHtml",
                                                loc,
                                                data: {
                                                    class: mainClass,
                                                    line: mainSelector.loc.start.line,
                                                    filePath: mainSelectorItem.path,
                                                },
                                            });
                                        } else {
                                            mainSelector.children
                                                ?.filter((child) =>
                                                    child.selectorWithoutPseudo
                                                        .split(",")
                                                        .find(
                                                            (child) =>
                                                                !ignoreSelector(child),
                                                        ),
                                                )
                                                .forEach((child) => {
                                                    child.selectorWithoutPseudo
                                                        .split(",")
                                                        .forEach((modifierClass) => {
                                                            if (
                                                                !htmlTree.tree
                                                                    .filter(
                                                                        (
                                                                            classFromHtml,
                                                                        ) =>
                                                                            classFromHtml.mainClass ===
                                                                            mainClass.substring(
                                                                                1,
                                                                            ),
                                                                    )
                                                                    .find(
                                                                        (
                                                                            classFromHtml,
                                                                        ) =>
                                                                            classFromHtml.modifiers.find(
                                                                                (
                                                                                    modifierFromHtml,
                                                                                ) =>
                                                                                    modifierFromHtml ==
                                                                                    modifierClass.substring(
                                                                                        2,
                                                                                    ),
                                                                            ),
                                                                    )
                                                            ) {
                                                                if (
                                                                    modifierClass.startsWith(
                                                                        "&.",
                                                                    )
                                                                ) {
                                                                    context.report({
                                                                        messageId:
                                                                            "modifierNotFoundInHtml",
                                                                        loc,
                                                                        data: {
                                                                            modifier: modifierClass,
                                                                            class: mainClass,
                                                                            filePath: mainSelectorItem.path,
                                                                            line:
                                                                            mainSelector
                                                                                .loc
                                                                                .start
                                                                                .line,
                                                                        },
                                                                    });
                                                                }
                                                            }
                                                        });
                                                });
                                        }
                                    });
                            })

                        });

                    });
                },
            }) as any;
        },
    });
}

export const noUnnecessaryCreateRule = (new noUnnecessaryRule).createESLintRule;
