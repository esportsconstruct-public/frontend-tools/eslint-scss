/**
 * @fileoverview test description
 * @author Peter Startsev
 */

"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------
import createForbiddenCombined from "./rules/forbidden-combined";
import {noUnnecessaryCreateRule} from "./rules/no-unnecessary-classes";
import createWrongScssModifierRule from "./rules/wrong-scss-modifier";
import createNoBingClass from "./rules/no-bind-class";
import createRequiredDirective from "./rules/required-directive-for-children";
import createForbiddenMixin from "./rules/forbidden-children-for-host";

//------------------------------------------------------------------------------
// Plugin Definition
//------------------------------------------------------------------------------

module.exports = {
    configs: {
        recommended: {
            parser: "@esportsconstruct-public/template-parser",
            plugins: ["@esportsconstruct-public/scss"],
            parserOptions: {
                excludeStartWith: ["g-", "ng-content-"]
            },
            rules: {
                "@esportsconstruct-public/scss/no-bind-class": "error",
                "@esportsconstruct-public/scss/forbidden-combined": "error",
                "@esportsconstruct-public/scss/wrong-scss-modifier": "error",
                "@esportsconstruct-public/scss/no-unnecessary-classes": [
                    "error",
                    {
                        modifierFromPseudo: true,
                        allowDeepNestedModifiers: true
                    }
                ]
            }
        }
    }
}


// import all rules in lib/rules
module.exports.rules = {
    'forbidden-combined': createForbiddenCombined,
    'no-bind-class': createNoBingClass,
    'wrong-scss-modifier': createWrongScssModifierRule,
    'no-unnecessary-classes': noUnnecessaryCreateRule,
    'required-directive-for-children': createRequiredDirective,
    'forbidden-children-for-host': createForbiddenMixin
};


// import processors
// module.exports.processors = requireIndex(__dirname + "/processors");
