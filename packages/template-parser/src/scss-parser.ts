import fs from "fs";
import {SourceLoc} from "./interfaces/sourceLoc";

export interface ScssTree {
    selector: string;
    selectorWithoutPseudo: string;
    loc: SourceLoc;
    // foundCombined?: {
    //     line: number;
    //     text: string;
    // };
    children?: ScssTree[];
}

export class ScssParser {
    public ignoreParsing = false;
    public tree: ScssTree[] = [];
    public readonly path: string;
    private readonly content: string;

    public constructor(path: string) {
        this.path = path;

        const fileContent = fs.readFileSync(this.path, "utf-8");

        if (fileContent.includes('eslint-disable @esportsconstruct-public/scss')) {
            this.ignoreParsing = true;
        }

        this.content = fileContent.replace(/\/\*.*?\*\//g, "");

        this.tree = this.parseStylesheet();
    }


    private parseStylesheet(): ScssTree[] {
        return this.ignoreParsing ? [] : this.parseBlock(this.content.split(/\r?\n/));
    }

    private parseBlock(block: string[]): ScssTree[] {
        let foundOpen = 0;
        let foundClosed = 0;
        let newBlock: string[] = [];
        let selectorName = "";
        const newElements: ScssTree[] = [];

        let foundCombined: {
            line: number;
            text: string;
        } | undefined;
        let index = 0;

        let combinedLines: string[] = [];

        for (let line of block) {
            if (line.trim() === '/* eslint-disable @esportsconstruct-public/scss */') {
                return [];
            }

            line = line.replace(/\/\/.*/g, "").trim();

            if (line === '') {
                index++;
                continue;
            }

            if (line.endsWith(',')) {
                if (foundOpen === 0) {
                    combinedLines.push(line.slice(0, -1).trim());

                }
                if (foundOpen === 1) {
                    newBlock.push(line);
                    continue;
                }

            }

            if (line.endsWith("{")) {
                foundOpen++;

                if (foundOpen === 1) {
                    selectorName = line.slice(0, -1).trim();

                    combinedLines.push(line.slice(0, -1).trim());

                    if (combinedLines.length) {
                        foundCombined = {
                            line: index + 1,
                            text: combinedLines.map(line => line.trim()).join(',')
                        };
                    }
                    combinedLines = [];
                }

            }
            if (line.endsWith("}")) {
                foundClosed++;
            }

            if (foundOpen === foundClosed) {
                if (line.endsWith("}")) {
                    foundOpen = 0;
                    foundClosed = 0;
                    combinedLines = [];

                    const pushObject: ScssTree = {
                        selector: selectorName,
                        selectorWithoutPseudo: selectorName.split(',').filter(line => !line.startsWith('&:')).join(','),
                        loc: {
                            start: { line: index + 1, col: 1 },
                            end: { line: index + 1, col: line.length - 1 }
                        }};
                    if (newBlock.length) {
                        pushObject.children = this.parseBlock([...newBlock]);
                    }
                    if (foundCombined) {
                        pushObject.selector = foundCombined.text;
                        pushObject.selectorWithoutPseudo = pushObject.selector.split(',').filter(line => !line.startsWith('&:')).join(',');
                        foundCombined = undefined;

                    }

                    newElements.push(pushObject);

                    newBlock = [];
                }
            } else if (foundOpen !== 1) {
                newBlock.push(line);
            }

            index++;
        }
        return newElements;
    }
}
