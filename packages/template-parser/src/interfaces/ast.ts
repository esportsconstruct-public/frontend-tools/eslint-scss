import {ParserNode} from "./parser-node";

export interface AST {
    type: 'Program',
    comments: string[];
    tokens: string[];
    range: [number, number];
    loc: {
        start: LOC;
        end: LOC;
    };
    templateNodes: ParserNode[];
    value: string;
}

export interface LOC {
    line: number;
    column: number;
}
