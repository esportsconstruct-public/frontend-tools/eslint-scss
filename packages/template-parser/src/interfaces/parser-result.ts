import { Scope, SourceCode} from "eslint";
import {AST, LOC} from "./ast";
import {ScssParser} from "../scss-parser";
import {ParseSourceSpan} from "@angular/compiler";
import {HtmlTree} from "../html-tree";

export interface ParseResult {
    ast: AST;
    services?: ParserServices;
    scopeManager?: Scope.ScopeManager;
    visitorKeys?: SourceCode.VisitorKeys;
}

export interface ParserServices {
    convertNodeSourceSpanToLoc: (sourceSpan: ParseSourceSpan) => {
        start: LOC;
        end: LOC;
    };
    defineTemplateBodyVisitor: (
        templateBodyVisitor: { [x: string]: Function },
        scriptVisitor?: { [x: string]: Function },
    ) => { [x: string]: Function };
    getScssParser: () => ScssParser[][];
    getHtmlTree: () => HtmlTree;
}
