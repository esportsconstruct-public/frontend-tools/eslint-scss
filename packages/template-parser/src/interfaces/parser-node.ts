import {Node} from "@angular/compiler/src/render3/r3_ast";
import {BindingType, ParseSourceSpan} from "@angular/compiler";

export interface ParserNode extends Node {
    name?: string;
    tagName?: string;
    type?: string | BindingType;
    angularBindingType?: BindingType;
    startSourceSpan?: ParseSourceSpan | null;
    endSourceSpan?: ParseSourceSpan | null;
    parent?: ParserNode | null;
    value?: {
        source: string;
    }
}
