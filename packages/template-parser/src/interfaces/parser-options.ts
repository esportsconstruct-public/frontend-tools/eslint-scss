export interface ParserOptions {
    filePath: string;
    templateSubfolders?: string[];
    excludeStartWith?: string[];
}
