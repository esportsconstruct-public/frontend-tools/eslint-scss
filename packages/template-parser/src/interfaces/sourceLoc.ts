export interface SourceLoc {
    start: { line: number; col: any };
    end: { line: number; col: any };
}
