import {BindingType, ParseSourceSpan, parseTemplate} from "@angular/compiler";
import EventEmitter from "events";
import {Scope, ScopeManager} from "eslint-scope";
// @ts-ignore
import NodeEventGenerator from "eslint/lib/linter/node-event-generator";
import {ScssParser} from "./scss-parser";
import {SourceCode} from "eslint";
import {AST, LOC} from "./interfaces/ast";
import {ParseResult} from "./interfaces/parser-result";
import {ParserNode} from "./interfaces/parser-node";
import {ParserOptions} from "./interfaces/parser-options";
import fs from "fs";
import * as path from "path";
import VisitorKeys = SourceCode.VisitorKeys;
import {HtmlTree} from "./html-tree";

type NodeVisitorFn = (node: ParserNode, parent: ParserNode | null) => void;


const emitters = new WeakMap();

/**
 * @see @angular/compiler/src/render3/r3_ast
 */
const allowedNodes: { [x: string]: string[] } = {
    // ASTWithSource: ['ast'],
    // Text: ['left', 'right'],
    // BoundText: ['value'],
    // TextAttribute: ['value'],
    // BoundAttribute: [''],
    // BoundEvent: ['handler'],
    Element: ["children", "attributes", "inputs"],
    Template: ["templateAttrs", "children", "inputs"],
    // Content: [""],
    // Variable: [""],
    // Reference: [""],
    // Icu: [""],
};

function getFallbackKeys(node: ParserNode): string[] {
    return Object.keys(node).filter((key: string) => {
        let value = null;
        return (
            key !== "comments" &&
            key !== "leadingComments" &&
            key !== "loc" &&
            key !== "parent" &&
            key !== "range" &&
            key !== "tokens" &&
            key !== "trailingComments" &&
            (value = node[key as keyof ParserNode]) !== null &&
            typeof value === "object"
            // (typeof value.type === "string" || Array.isArray(value))
        );
    });
}

function isNode(x: any): x is ParserNode {
    return x !== null && typeof x === "object";
}


function traverse(
    node: ParserNode,
    parent: ParserNode | null,
    visitor: {
        visitorKeys: VisitorKeys;
        enterNode: NodeVisitorFn;
        leaveNode: NodeVisitorFn;
    },
) {
    let i = 0;
    let j = 0;

    visitor.enterNode(node, parent);

    const keys =
        (node.type && (visitor.visitorKeys || allowedNodes)[node.type]) || getFallbackKeys(node);

    for (i = 0; i < keys.length; ++i) {
        const child: unknown = node[keys[i] as keyof ParserNode];
        const isArr = Array.isArray(child);
        if (isArr) {
            for (j = 0; j < (child as ParserNode[]).length; ++j) {
                const c = (child as ParserNode[])[j];
                if (isNode(c)) {
                    traverse(c, node, visitor);
                }
            }
        } else if (isNode(child)) {
            traverse(child, node, visitor);
        }
    }

    visitor.leaveNode(node, parent);
}

/**
 * Need all Nodes to have a `type` property before we begin
 */
function preprocessNode(node: ParserNode) {
    let i = 0;
    let j = 0;

    const keys: string[] = (node && node.type && allowedNodes[node.type]) || getFallbackKeys(node);

    for (i = 0; i < keys.length; ++i) {
        const child: unknown = node[keys[i] as keyof ParserNode];
        if (Array.isArray(child)) {
            for (j = 0; j < (child as ParserNode[]).length; ++j) {
                prepareNodeElement((child as ParserNode[])[j] as ParserNode);
            }
        } else {
            prepareNodeElement(child as ParserNode);
        }
    }
}

function prepareNodeElement(node: ParserNode) {
    // Angular sometimes uses a prop called type already
    if (isNode(node)) {
        if (node.type !== undefined && node.angularBindingType === undefined) {
            node.angularBindingType = node.type as BindingType;
        }
        if (!node.type) {
            node.type = node.constructor.name;
        }
        preprocessNode(node);
    }
}


function convertNodeSourceSpanToLoc(sourceSpan: ParseSourceSpan): {
    start: LOC;
    end: LOC;
} {
    return {
        start: {
            line: sourceSpan.start.line + 1,
            column: sourceSpan.start.col,
        },
        end: {
            line: sourceSpan.end.line + 1,
            column: sourceSpan.end.col,
        },
    };
}

function getStartSourceSpanFromAST(ast: AST): ParseSourceSpan | null {
    let startSourceSpan: ParseSourceSpan | null = null;
    ast.templateNodes.forEach((node) => {
        const nodeSourceSpan = node.startSourceSpan || node.sourceSpan;

        if (!startSourceSpan) {
            startSourceSpan = nodeSourceSpan;
            return;
        }

        if (
            nodeSourceSpan &&
            nodeSourceSpan.start.offset < startSourceSpan.start.offset
        ) {
            startSourceSpan = nodeSourceSpan;
            return;
        }
    });
    return startSourceSpan;
}

function getEndSourceSpanFromAST(ast: AST): ParseSourceSpan | null {
    let endSourceSpan: ParseSourceSpan | null = null;
    ast.templateNodes.forEach((node) => {
        const nodeSourceSpan = node.endSourceSpan || node.sourceSpan;

        if (!endSourceSpan) {
            endSourceSpan = nodeSourceSpan;
            return;
        }

        if (
            nodeSourceSpan &&
            nodeSourceSpan.end.offset > endSourceSpan.end.offset
        ) {
            endSourceSpan = nodeSourceSpan;
            return;
        }
    });
    return endSourceSpan;
}

function parseForESLint(code: string, options: ParserOptions): ParseResult {
    const angularCompilerResult = parseTemplate(code, options.filePath, {
        preserveWhitespaces: false,
    });

    const ast: AST = {
        type: "Program",
        comments: [],
        tokens: [],
        range: [0, 0],
        loc: {
            start: {line: 0, column: 0},
            end: {line: 0, column: 0},
        },
        templateNodes: angularCompilerResult.nodes,
        value: code,
    };

    /**
     * The types for ScopeManager seem to be wrong, it requires an configuration object argument
     * or it will throw at runtime
     */
        // @ts-ignore
    const scopeManager = new ScopeManager({});
    /**
     * Create a global scope for the ScopeManager, the types for Scope also
     * seem to be wrong
     */
    // @ts-ignore
    new Scope(scopeManager, "module", null, ast, false);

    preprocessNode(ast as unknown as ParserNode);

    const startSourceSpan = getStartSourceSpanFromAST(ast);
    const endSourceSpan = getEndSourceSpanFromAST(ast);

    if (startSourceSpan && endSourceSpan) {
        ast.range = [startSourceSpan.start.offset, endSourceSpan.end.offset];
        ast.loc = {
            start: convertNodeSourceSpanToLoc(startSourceSpan).start,
            end: convertNodeSourceSpanToLoc(endSourceSpan).end,
        };
    }

    /**
     * For folder scructure
     * project1
     *   comp.scss
     * project2
     *   comp.scss
     * comp.scss
     * comp.html
     *
     * scssParser = [[comp.scss, project1/comp.css], [comp.scss, project2/comp.scss]]
     */
    const scssParsers: ScssParser[][] = [];
    try {
        /**
         * For folder scructure
         * project1
         *   comp.scss
         * project2
         *   comp.scss
         * comp.scss
         * comp.html
         *
         * scssParser = [[comp.scss, project1/comp.css], [comp.scss, project2/comp.scss]]
         */

        let scssPath = options.filePath.replace("html", "scss");

        if (options.templateSubfolders) {
            const existSubfolders = options.templateSubfolders.filter(folder => {
                const pathWithSubfolder = `${path.dirname(scssPath)}/${folder}/${path.basename(scssPath)}`;
                return fs.existsSync(pathWithSubfolder)
            })
            if (existSubfolders.length > 0) {
                existSubfolders.forEach((folder) => {
                    const pathWithSubfolder = `${path.dirname(scssPath)}/${folder}/${path.basename(scssPath)}`;
                    if (fs.existsSync(pathWithSubfolder)) {
                        const parsers = [];
                        if (fs.existsSync(scssPath)) {
                            parsers.push(new ScssParser(scssPath));
                        }

                        if (fs.existsSync(pathWithSubfolder)) {
                            parsers.push(new ScssParser(pathWithSubfolder));
                        }

                        scssParsers.push(parsers);
                    }
                });
            } else {
                const parsers = [];
                if (fs.existsSync(scssPath)) {
                    parsers.push(new ScssParser(scssPath));
                }
                scssParsers.push(parsers);
            }
        } else {
            const parsers = [];
            if (fs.existsSync(scssPath)) {
                parsers.push(new ScssParser(scssPath));
            }
            scssParsers.push(parsers);
        }
    } catch (_e) {
    }

    return {
        ast,
        scopeManager,
        visitorKeys: allowedNodes,
        services: {
            convertNodeSourceSpanToLoc,
            defineTemplateBodyVisitor(
                templateBodyVisitor: { [x: string]: Function },
                scriptVisitor?: { [x: string]: Function },
            ) {
                const rootAST = ast;
                if (scriptVisitor == null) {
                    scriptVisitor = {}; //eslint-disable-line no-param-reassign
                }
                if (rootAST == null) {
                    return scriptVisitor;
                }

                let emitter = emitters.get(rootAST);

                // If this is the first time, initialize the intermediate event emitter.
                if (emitter == null) {
                    emitter = new EventEmitter();
                    emitter.setMaxListeners(0);
                    emitters.set(rootAST, emitter);

                    const programExitHandler = scriptVisitor["Program:exit"];
                    scriptVisitor["Program:exit"] = (node: ParserNode) => {
                        try {
                            if (typeof programExitHandler === "function") {
                                programExitHandler(node);
                            }

                            // Traverse template body.
                            const generator = new NodeEventGenerator(emitter);
                            traverse(rootAST as unknown as ParserNode, null, generator);
                        } finally {
                            // @ts-ignore
                            scriptVisitor["Program:exit"] = programExitHandler;
                            emitters.delete(rootAST);
                        }
                    };
                }

                // Register handlers into the intermediate event emitter.
                for (const selector of Object.keys(templateBodyVisitor)) {
                    emitter.on(selector, templateBodyVisitor[selector]);
                }

                return scriptVisitor;
            },
            getScssParser(): ScssParser[][] {
                return scssParsers;
            },
            getHtmlTree(): HtmlTree {
                return new HtmlTree(angularCompilerResult.nodes, options.excludeStartWith ?? []);
            },
        },
    };
}

module.exports = {
    parseForESLint,
    parse: function parse(code: string, options: { filePath: string }) {
        return parseForESLint(code, options).ast;
    },
};
