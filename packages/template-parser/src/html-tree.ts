import { Element, Node } from "@angular/compiler/src/render3/r3_ast";
import { BindingType } from "@angular/compiler/src/expression_parser/ast";

export interface HtmlTreeItem {
    mainClass: string;
    modifiers: string[];
    elements: Array<{
        attributes: string[];
        node: Element;
    }>;
}

export class HtmlTree {
    public tree: HtmlTreeItem[] = [];
    public readonly excludeStartWith: string[] = [];

    public constructor(nodes: Node[], excludeStartWith?: string[]) {
        this.excludeStartWith = excludeStartWith ?? [];

        this.parseNodes(nodes as Element[]);
    }

    private parseNodes(nodes: Element[]): void {
        nodes.forEach((node) => {
            const treeItem = this.getTreeItem(node as Element & {type: string});
            if (treeItem) {
                const foundTreeItem = this.tree.find(item => item.mainClass === treeItem.mainClass)
                if (foundTreeItem){
                    foundTreeItem.modifiers = [...foundTreeItem.modifiers, ...treeItem.modifiers];
                    foundTreeItem.elements = [...foundTreeItem.elements, ...treeItem.elements]
                } else {
                    this.tree.push(treeItem);
                }
            }

            if (node.children?.length) {
                this.parseNodes(node.children as Element[]);
            }
        });
    }

    private getTreeItem(element: Element & {type: string}): HtmlTreeItem | undefined {
        if (element.attributes && element.type !== "Template") {
            const node = element.attributes.find(
                (attribute) => attribute.name === "class"
            );

            if (node) {
                const originalClasses: string[] = node.value.split(" ");
                let classes: string[] = originalClasses;

                const ignoreFromOptions: string[] | null =
                    this.excludeStartWith ?? [];
                if (ignoreFromOptions) {
                    classes = classes.filter(
                        (item) =>
                            !ignoreFromOptions.find((ignore) =>
                                item.startsWith(ignore)
                            )
                    );
                }

                const HtmlMainClass: string = classes.length ? classes[0] : "";

                if (!HtmlMainClass) {
                    return undefined;
                }

                const foundClassInputs = element.inputs.filter(
                    (input) => input.type === BindingType.Class
                );
                if (foundClassInputs?.length) {
                    classes = [
                        ...classes,
                        ...foundClassInputs.map((input) => input.name),
                    ];
                }

                const foundAttributeRouterLinkActive = element.attributes.find(
                    (input) => input.name === "routerLinkActive"
                );
                if (foundAttributeRouterLinkActive) {
                    classes = [
                        ...classes,
                        foundAttributeRouterLinkActive.value,
                    ];
                }

                const foundInputRouterLinkActive = element.inputs.find(
                    // @ts-ignore
                    (input) =>
                        (input as unknown as {angularBindingType: BindingType}).angularBindingType === BindingType.Property &&
                        input.name === "routerLinkActive"
                );
                const foundInputRouterLinkActiveValue =
                    foundInputRouterLinkActive?.value;
                if (foundInputRouterLinkActiveValue) {
                    classes = [
                        ...classes,
                        ((foundInputRouterLinkActiveValue as unknown) as {
                            source: string;
                        }).source.replace(/'/gu, ""),
                    ];
                }

                const HtmlModifiers: string[] = classes.length
                    ? classes.slice(1)
                    : [];

                return {
                    mainClass: HtmlMainClass,
                    modifiers: HtmlModifiers,
                    elements: [{
                        attributes: element.attributes.map(
                            (attribute) => attribute.name
                        ),
                        node: element,
                    }]
                };
            }
        }
        return undefined;
    }
}
