import { ScssParser, ScssTree } from "./scss-parser";

describe("Scss parser", () => {
    let master = new ScssParser("src/mock/test.scss");
    let scssTree: ScssTree[];

    beforeEach(() => {
        scssTree = master.tree;
    });

    it("Check structure", () => {
        expect(scssTree).toEqual([
            {
                children: [
                    {
                        children: [
                            {
                                loc: {
                                    end: {
                                        col: 8,
                                        line: 6,
                                    },
                                    start: {
                                        col: 1,
                                        line: 6,
                                    },
                                },
                                selector:
                                    "&.active-inside,&.active-inside-2,&:hover",
                                selectorWithoutPseudo:
                                    "&.active-inside,&.active-inside-2",
                            },
                        ],
                        loc: {
                            end: {
                                col: 4,
                                line: 9,
                            },
                            start: {
                                col: 1,
                                line: 9,
                            },
                        },
                        selector: "&:active,&:hover,&.active-2",
                        selectorWithoutPseudo: "&.active-2",
                    },
                    {
                        loc: {
                            end: {
                                col: 4,
                                line: 12,
                            },
                            start: {
                                col: 1,
                                line: 12,
                            },
                        },
                        selector: "&.modifier",
                        selectorWithoutPseudo: "&.modifier",
                    },
                    {
                        loc: {
                            end: {
                                col: 4,
                                line: 15,
                            },
                            start: {
                                col: 1,
                                line: 15,
                            },
                        },
                        selector: ".wrong-modifier",
                        selectorWithoutPseudo: ".wrong-modifier",
                    },
                ],
                loc: {
                    end: {
                        col: 0,
                        line: 25,
                    },
                    start: {
                        col: 1,
                        line: 25,
                    },
                },
                selector: ".container",
                selectorWithoutPseudo: ".container",
            },
            {
                children: [
                    {
                        loc: {
                            end: {
                                col: 4,
                                line: 2,
                            },
                            start: {
                                col: 1,
                                line: 2,
                            },
                        },
                        selector: ".x",
                        selectorWithoutPseudo: ".x",
                    },
                ],
                loc: {
                    end: {
                        col: 0,
                        line: 31,
                    },
                    start: {
                        col: 1,
                        line: 31,
                    },
                },
                selector: ".container-2",
                selectorWithoutPseudo: ".container-2",
            },
        ]);
    });
});
