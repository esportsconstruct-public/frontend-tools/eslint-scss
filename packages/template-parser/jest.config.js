"use strict";

module.exports = {
    roots: ["./"],
    testPathIgnorePatterns: ["/node_modules/", "/dist/"],
    cacheDirectory: ".cache/jest",
    preset: 'ts-jest',
    globals: {
        'ts-jest': {
            tsConfig: 'tsconfig.spec.json'
        }
    }
};
