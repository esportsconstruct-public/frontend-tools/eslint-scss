# Template parser

Parser for Eslint. Serves for the analysis of HTML and CSS files for Angular projects.

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```
Next add .npmrc file to root directory for your project (or add new line if you have already)
We use gilab npm registry, your npm needs to know where to find the package

```
@esportsconstruct-public:registry=https://gitlab.com/api/v4/packages/npm/
```

Next, install `template-parser` and `eslint-plugin-scss`:

```
$ npm install @esportsconstruct-public/template-parser --save-dev
```

## How it works

The plugin reacts to the *component.html files, receives a tree of elements from the compiler's angular.
Searches for a *component.scss file in the neighborhood and determines its structure.

Inspired by this [angular-eslint](https://github.com/angular-eslint/angular-eslint/tree/master/packages/template-parser)

The main differences:
* Calling rules can find scssParser class in its context
* Calling rules can find htmlTree class in its context

## Options

You can use special options to find css files. regarding html.

Example folder structure
```
comp1.html
comp1.scss
project1
    comp1.scss
project2
    comp1.scss
```

```js
parserOptions: {
    templateSubfolders: ["project1", "project2"]
}
```

In this case, the files will be scanned together (first pair: comp1.scss and project1/comp1.scss, second pair: comp1.scss and projects2/comp1.scss). If one of the rules is PASSED in at least one of the pairs. The rule is considered passed
